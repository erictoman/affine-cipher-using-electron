const testFolder = './ORIGINAL/';
const testFolder2 = './MODIFIED/';
var $ = require('jquery')
const fs = require("fs")
var image=""
var shift
var key0
var x_e

function ModImage(key0,file,shift,modo){
    let data=fs.readFileSync(image)
    for(var i=0;i<data.length;i++){
        data[i]=affine(key0,data[i],shift,modo)
    }
    fs.writeFileSync(testFolder2+file,data,function(err){

    })
    alert("Ready!")
}
function affine(key,code,shift,modo){
    if(modo){
        return  (key*code+shift)%256
    }else{
        return  x_e*(code-shift)%256
    }
    
}

function xgcd(a, b) {
    if (b == 0) {
      return [1, 0, a];
    }
    temp = xgcd(b, a % b);
    x = temp[0];
    y = temp[1];
    d = temp[2];
    return [y, x-y*Math.floor(a/b), d];
}

$(() => {
        $("#ENC").click(function(){
            if(checkKeys()){
                fs.readdirSync(testFolder).forEach(file => {
                    image=testFolder+file
                    ModImage(key0,file,shift,1)
                })
            }else{
                alert("Not valid key")
            }
        })
        $("#DEC").click(function(){
            if(checkKeys()){
                fs.readdirSync(testFolder).forEach(file => {
                    image=testFolder+file
                    ModImage(key0,file,shift,0)
                })
            }else{
                alert("Not valid key")
            }
        })
})
function checkKeys(){
    key0 = parseInt($("#key0").val())
    shift = parseInt($("#key1").val())
    let [x,y,gdc]=xgcd(key0,256)
    console.log(gdc)
    if(gdc==1){
        x_e=x
        return true
    }
    return false
}